import 'bootstrap/dist/css/bootstrap.min.css';
import NavMenu from './components/NavMenu';
import {BrowserRouter as Router, Route,Switch }from 'react-router-dom'
import HomeLink from './views/HomeLink';
import VideoLink from './views/VideoLink';
import AccountLink from './views/AccountLink';
import Welcome from './views/Welcome';
import Auth from './views/Auth';
import React, { Component } from 'react'

export default class App extends Component {

  render() {
    return (
      <div>
      <Router>
      <NavMenu/>
        <Switch>
          <Route exact path='/home' component={HomeLink}/>
          <Route path='/video' component={VideoLink}/>
          <Route path='/account' component={AccountLink}/>
          <Route path='/welcome' component={Welcome}/>
          <Route path='/auth' component={Auth}/>
        </Switch>
      </Router>
    </div>
    )
  }
}

