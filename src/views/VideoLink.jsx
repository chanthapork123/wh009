import React from 'react'
import { Container } from 'react-bootstrap'
import VideoForm from '../components/VideoForm'


export default function VideoLink() {
    return (
        <Container>
            <VideoForm/>
        </Container>
    )
}
