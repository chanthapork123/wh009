import React from 'react'
import { Col, Container, Row} from 'react-bootstrap'

import { Component } from 'react'
import CardForm from '../components/CardForm';

export default class HomeLink extends Component {

    constructor(){
        super();
        this.state={
          data:[{
            img :'img/1.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          },
          {
            img :'img/2.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          },
          {
            img :'img/3.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          },
          {
            img :'img/4.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          },
          {
            img :'img/5.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          },
          {
            img :'img/6.jpeg',
            title:'Seven Deadny Sins',
            about: 'Content'
          }
          ]
        }
      }
    render() {
        return (
            <Container style={{paddingTop:'15px'}}>
                <Row>
                    {
                    this.state.data.map((obj,index)=><Col md={3}> <CardForm data={obj} key={index}/></Col>)
                    }   
                </Row>
                
        </Container>
        )
    }
}

