import React from 'react'
import { Container } from 'react-bootstrap'

export default function Welcome() {
    return (
        <Container>
           <h4>Welcome</h4>
        </Container>
    )
}
