import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import DemoForm from '../components/DemoForm'

export default function Auth() {
    return (
        <Container style={{paddingTop:'15px'}}>
            <Row>
                <Col md={6}>
                    <DemoForm/>
                </Col>
            </Row>
        </Container>
    )
}
