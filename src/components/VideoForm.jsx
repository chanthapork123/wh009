import React from 'react'
import {ButtonGroup,Button, Container} from 'react-bootstrap'
export default function VideoForm() {
    return (
        <Container>
            <h4>Video</h4>
            <ButtonGroup aria-label="Basic example">
                <Button variant="secondary">Movie</Button>
                <Button variant="dark">Animation</Button>
            </ButtonGroup>
        </Container>
    )
}
