import React from 'react'
import {  Container,Card,Button} from 'react-bootstrap'

export default function CardForm({data}) {
    return (
        <Container style={{paddingTop:'15px'}}>
                <Card > 
                    <Card.Img variant="top" src={data.img}/>
                    <Card.Body>
                        <Card.Title>{data.title}</Card.Title>
                        <Card.Text>{data.about}</Card.Text>
                        <Button variant="info">Read</Button>
                    </Card.Body>
                </Card>
        </Container>
    )
}
