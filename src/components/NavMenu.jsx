import React from 'react'
import { Container,Navbar,Nav,Form,FormControl,Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

export default function NavMenu() {
    return (
        <Container>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#">React-Router</Navbar.Brand>
                    <Nav className="mr-auto my-2 my-lg-0" style={{ maxHeight: '100px' }} navbarScroll >
                    <Nav.Link as={NavLink} to='/home'>Home</Nav.Link>
                    <Nav.Link as={NavLink} to='/video'>Video</Nav.Link>
                    <Nav.Link as={NavLink} to='/account'>Account</Nav.Link>
                    <Nav.Link as={NavLink} to='/welcome'>Welcome</Nav.Link>
                    <Nav.Link as={NavLink} to='/auth'>Auth</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                    <FormControl
                        type="search"
                        placeholder="Search"
                        className="mr-2"
                        aria-label="Search"
                    />
                    <Button variant="outline-success">Search</Button>
                    </Form>
            </Navbar>
        </Container>
    )
}
