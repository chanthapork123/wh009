import React from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router,Route,Switch,Link} from 'react-router-dom'
import Child from './Child'


export default function AccoutForm() {
    return (
        <Container>
        <Router>
      <div>
        <h2>Accounts</h2>
        <ul>
          <li><Link to='/netflix'>Netflix</Link></li>
          <li><Link to='/zillow-group'>Zillow Group</Link></li>
          <li><Link to='/yahoo'>Yahoo</Link></li>
          <li><Link to='/modus-create'>Modus Create</Link></li>
        </ul>
        <Switch>
          <Route path="/:id" render={()=> <Child/>} />
        </Switch>
      </div>
    </Router>
    </Container>
    )
}

